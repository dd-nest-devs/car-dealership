import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Post,
} from '@nestjs/common';
import { CarsService } from './cars.service';

@Controller('cars')
export class CarsController {
  constructor(private readonly carsService: CarsService) {}

  @Get()
  getAllCars() {
    return this.carsService.findAll();
  }

  @Get('/:id')
  getCarByID(@Param('id', ParseIntPipe) id: number) {
    return {
      car: this.carsService.findOneById(id),
    };
  }

  @Post()
  createCar(@Body() body: any) {
    return {
      success: true,
      body,
    };
  }

  @Patch('/:id')
  updateCar(@Param('id', ParseIntPipe) id: number, @Body() body: any) {
    return {
      success: true,
      body,
      id,
    };
  }

  @Delete('/:id')
  deleteCar(@Param('id', ParseIntPipe) id: number) {
    return {
      success: true,
      id,
    };
  }
}
